// $Id: shadowwindow.h,v 1.1 2002/05/29 22:06:50 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef SHADOWWINDOW_H__9739a257_5c6d_431a_a177_b2c16294366c___INCLUDED
#define SHADOWWINDOW_H__9739a257_5c6d_431a_a177_b2c16294366c___INCLUDED

class CSwitcherWindow;

class CShadowWindow : public CWindowImpl <CShadowWindow>
{
public:
  CShadowWindow(CSwitcherWindow *pWndSource);
  ~CShadowWindow();

  void Move(unsigned int x, unsigned int y);

  BEGIN_MSG_MAP(CShadowWindow)
    MESSAGE_HANDLER(WM_PAINT,OnPaint)
  END_MSG_MAP()

protected:
  LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

private:
  int m_nWidth;
  int m_nHeight;
  CSwitcherWindow *m_pWndSource;
};

#endif // #ifdef SHADOWWINDOW_H__9739a257_5c6d_431a_a177_b2c16294366c___INCLUDED
