// $Id: desktopswitcher.h,v 1.1 2002/05/29 22:06:50 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef _DESKTOP_SWITCHER_H_05586d56_2e6c_4b3f_8199_9e1e5f680410__INCLUEDED
#define _DESKTOP_SWITCHER_H_05586d56_2e6c_4b3f_8199_9e1e5f680410__INCLUEDED

class CDesktop;

class CDesktopSwitcher
{
public:
  CDesktopSwitcher();
  ~CDesktopSwitcher();
  HRESULT Go(unsigned int nDesktops);
  void Exit();
  CDesktop * GetDefaultDesktop();
  void RegisterHotKeys(HWND hWnd);
  HRESULT HotKeyPressed(WORD wVKey, WORD wModifiers);
private:
  CDesktop **m_ppDesktops;
  HANDLE m_hExitEvent;
  unsigned int m_nDesktops;
  unsigned int m_nCurrentDesktopIndex;
};

#endif // #ifndef _DESKTOP_SWITCHER_H_05586d56_2e6c_4b3f_8199_9e1e5f680410__INCLUEDED
