// $Id: ph.h,v 1.2 2002/06/29 15:03:33 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef _PH_H_16963f56_730d_4cce_9591_bad0ae574a77_INCLUDED
#define _PH_H_16963f56_730d_4cce_9591_bad0ae574a77_INCLUDED

#define _WIN32_WINNT 0x0500
#define WINVER 0x0500

#include <windows.h>
#include <stdio.h>

#include <atlbase.h>

extern CComModule _Module;

#include <atlwin.h>
#include <atlcom.h>

#include <assert.h>

#ifdef _DEBUG
#define ASSERT(x) assert(x)
#define VERIFY(x) assert(x)
#else
#define ASSERT(x) (void(0))
#define VERIFY(x) (x);
#endif

#include <png.h>

#endif // ifndef _PH_H_16963f56_730d_4cce_9591_bad0ae574a77_INCLUDED
