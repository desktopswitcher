// $Id: desktop.cpp,v 1.1 2002/05/29 22:06:50 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "ph.h"
#include "Desktop.h"
#include "switcher.h"
#include "SwitcherWindow.h"

CDesktop::CDesktop(CDesktopSwitcher *pDesktopSwitcher, const char *pszDesktopName, const char *pszDesktopShowName, HRESULT& rhrError)
{
  rhrError = E_UNEXPECTED;
  m_hDesktop = NULL;
  m_pWnd = NULL;
  m_pDesktopSwitcher = NULL;
  m_pszDesktopShowName = NULL;
  m_pszDesktopName = NULL;

  m_pszDesktopName = new char [strlen(pszDesktopName)+1];
  if (!m_pszDesktopName)
  {
    rhrError = E_OUTOFMEMORY;
    return;
  }

  strcpy(m_pszDesktopName,pszDesktopName);

  m_pszDesktopShowName = new char [strlen(pszDesktopShowName)+1];
  if (!m_pszDesktopShowName)
  {
    rhrError = E_OUTOFMEMORY;
    return;
  }

  strcpy(m_pszDesktopShowName,pszDesktopShowName);

  // First, try to open an existing desktop
  m_hDesktop = OpenDesktop(m_pszDesktopName, 0, FALSE, GENERIC_ALL);
  if (!m_hDesktop)
  {
    // Failing an open, create it
    m_hDesktop = CreateDesktop(m_pszDesktopName,
                               NULL,
                               NULL,
                               0,
                               MAXIMUM_ALLOWED,
                               NULL);

    if (!m_hDesktop)
    {
      rhrError = HRESULT_FROM_WIN32(GetLastError());
      return;
    }
  }

  m_pDesktopSwitcher = pDesktopSwitcher;

  DWORD tID;
  HANDLE hThread = CreateThread(NULL,
                                0,
                                ThreadProc,
                                this,
                                0,
                                &tID);

  if (!hThread)
  {
    rhrError = HRESULT_FROM_WIN32(GetLastError());
    return;
  }

  VERIFY(CloseHandle(hThread));

  rhrError = S_OK;
}

CDesktop::~CDesktop()
{
  ASSERT(!m_hDesktop);
  if (m_hDesktop)
    VERIFY(CloseDesktop(m_hDesktop));

  if (m_pszDesktopName)
    delete m_pszDesktopName;

  if (m_pszDesktopShowName)
    delete m_pszDesktopShowName;

  if (m_pWnd)
    delete m_pWnd;
}

DWORD WINAPI CDesktop::ThreadProc(LPVOID lpParameter)
{
  return ((CDesktop *)lpParameter)->ThreadProc();
}

DWORD CDesktop::ThreadProc()
{
  DWORD dwRet = -1;

  USEROBJECTFLAGS uof; // To set Desktop attributes

  uof.fInherit = FALSE;        // If an app inherits multiple desktop handles,
  // it could run on any one of those desktops
  uof.fReserved = FALSE;
  // Let other account processes hook this desktop
  uof.dwFlags = DF_ALLOWOTHERACCOUNTHOOK;
  SetUserObjectInformation(m_hDesktop,
                           UOI_FLAGS,
                           (LPVOID)&uof,
                           sizeof(uof));

  // Assign new desktop to this thread
  SetThreadDesktop (m_hDesktop);

  m_pWnd = new CSwitcherWindow(m_pDesktopSwitcher,this);
  if (!m_pWnd)
  {
    dwRet = ERROR_OUTOFMEMORY;
    goto Exit;
  }

  if (!m_pWnd->Create())
  {
    dwRet = GetLastError();
    goto Exit;
  }

  // The message loop
  MSG msg;
  while (((dwRet = GetMessage(&msg, NULL,  0, 0)) != 0) && (dwRet != -1))
  {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }

  m_pWnd->Destroy();

Exit:
  if (m_pWnd)
    delete m_pWnd;
  VERIFY(CloseDesktop(m_hDesktop));

  delete this;
  return dwRet;
}

// Called by CDesktopSwitcher only !!!
void CDesktop::Exit()
{
  PostQuitMessage(0);           // we don't use the parameter
}

HRESULT CDesktop::SwitchTo()
{
  if (!SwitchDesktop(m_hDesktop))
  {
    DWORD dwError = GetLastError();
    return HRESULT_FROM_WIN32(dwError);
  }

  return S_OK;
}

void CDesktop::WindowToggleVisible()
{
  if (m_pWnd)
    m_pWnd->ToggleVisible();
}

void CDesktop::Run(const char *pszCommand)
{
  STARTUPINFO sui;         // Process startup info
  PROCESS_INFORMATION pi;  // info returned from CreateProcess

  // Most sui members will be 0
  //
  ZeroMemory ((PVOID)&sui, sizeof(sui));

  sui.cb = sizeof (sui);

  TCHAR *pszCommandBuffer = new char [strlen(pszCommand)+1];
  if (!pszCommandBuffer)
  {
    ::MessageBox(NULL,"Out of memory", "Error", MB_OK|MB_ICONSTOP);
    return;
  }

  strcpy(pszCommandBuffer,pszCommand);

  TCHAR *pszDesktopName = new char [strlen(m_pszDesktopName)+1];
  if (!pszDesktopName)
  {
    ::MessageBox(NULL,"Out of memory", "Error", MB_OK|MB_ICONSTOP);
    delete pszCommandBuffer;
    return;
  }

  strcpy(pszDesktopName,m_pszDesktopName);

  sui.lpDesktop = pszDesktopName;
  if (!CreateProcess( NULL,   // image name
                      pszCommandBuffer, // command line
                      NULL,   // process security attributes
                      NULL,   // thread security attributes
                      TRUE,   // inherit handles
                      CREATE_DEFAULT_ERROR_MODE|CREATE_SEPARATE_WOW_VDM,
                      NULL,   // environment block
                      NULL,   // current directory
                      &sui,   // STARTUPINFO
                      &pi))   // PROCESS_INFORMATION
  {
    char Buffer[1024];
    sprintf(Buffer,"CreateProcess failed. GetLastError() returns 0x%X",GetLastError());
    ::MessageBox(NULL, Buffer, "Error", MB_OK|MB_ICONSTOP);
  }

  delete pszCommandBuffer;
  delete pszDesktopName;
}

const char *CDesktop::GetDesktopName()
{
  return m_pszDesktopShowName;
}

void CDesktop::RunPrompt()
{
  if (m_pWnd)
    m_pWnd->OnRun();
}
