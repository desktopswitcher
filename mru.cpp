// $Id: mru.cpp,v 1.1 2002/05/29 22:06:50 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "ph.h"
#include "MRU.h"

#define ENTRY_BASE 16

CMRUList::CMRUList(unsigned int nMaxEntries, LPCTSTR psz_HKCU_MRUKey, HRESULT& rhr)
{
  m_nCount = 0;
  m_hKey = NULL;
  m_nCount = 0;
  m_nMaxEntries = nMaxEntries;

  if (nMaxEntries == 0 || psz_HKCU_MRUKey == NULL)
  {
    rhr = E_INVALIDARG;
    return;
  }

  m_ppszList = new LPTSTR[nMaxEntries];
  if (!m_ppszList)
  {
    rhr = E_OUTOFMEMORY;
    return;
  }

  ZeroMemory(m_ppszList,nMaxEntries*sizeof(LPTSTR));

  LONG nError;

  // open/create the key
  nError = RegCreateKeyEx(HKEY_CURRENT_USER, psz_HKCU_MRUKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE|KEY_SET_VALUE,NULL,&m_hKey,NULL);
  if (nError != ERROR_SUCCESS)
  {
    rhr = HRESULT_FROM_WIN32(nError);
    return;
  }

  TCHAR NameBuffer[256];

  DWORD dwType;
  DWORD dwEntrySize;

  while(m_nCount < m_nMaxEntries)
  {
    _ultot(m_nCount,NameBuffer,ENTRY_BASE);
    nError = RegQueryValueEx(m_hKey,NameBuffer,NULL,&dwType,NULL,&dwEntrySize);
    if (nError != ERROR_SUCCESS || dwType != REG_SZ)
      break;

    m_ppszList[m_nCount] = (TCHAR *) new BYTE[dwEntrySize];
    if (!m_ppszList)
    {
      rhr = E_OUTOFMEMORY;
      return;
    }

    nError = RegQueryValueEx(m_hKey,NameBuffer,NULL,&dwType,(BYTE *)m_ppszList[m_nCount],&dwEntrySize);
    if (nError != ERROR_SUCCESS)
    {
      delete m_ppszList[m_nCount];
      m_ppszList[m_nCount] = NULL;
      break;
    }

    m_nCount++;
  }
}

CMRUList::~CMRUList()
{
  if (m_ppszList)
  {
    LPTSTR *ppsz = m_ppszList;
    while(ppsz)
      delete ppsz++;

    delete m_ppszList;
  }

  if (m_hKey)
  {
    VERIFY(RegCloseKey(m_hKey) == ERROR_SUCCESS);
    m_hKey = NULL;
  }
}

HRESULT CMRUList::Add(LPCTSTR pszEntry)
{
  if (m_hKey == NULL || m_ppszList == NULL || m_nMaxEntries == 0 || m_nCount > m_nMaxEntries)
  {
    ASSERT(FALSE);              // Add method must be called only after successful construction
    return E_UNEXPECTED;
  }

  unsigned int nIndex;
  LPTSTR pszMoveTemp;
  HRESULT hr;

  // existing string ???
  for (nIndex = 0 ; nIndex < m_nCount ; nIndex++)
    if (strcmp(m_ppszList[nIndex],pszEntry) == 0)
    {
      if (nIndex != 0)
      { // move the found entry to the top
        pszMoveTemp = m_ppszList[nIndex]; // save the found entry
        while(nIndex)
        {
          m_ppszList[nIndex] = m_ppszList[nIndex-1];
          nIndex--;
        }

        m_ppszList[0] = pszMoveTemp;

        hr = WriteToRegistry();

        return SUCCEEDED(hr)?S_FALSE:hr;
      }

      return S_FALSE;
    }

  {
    // copy new entry
    LPTSTR pszNewEntry = new TCHAR[_tcslen(pszEntry)+1];
    if (!pszNewEntry)
      return E_OUTOFMEMORY;

    _tcscpy(pszNewEntry,pszEntry);

    nIndex = 0;
    LPTSTR psz = pszNewEntry;

    // move old entries
    while(nIndex < m_nCount)
    {
      pszMoveTemp = m_ppszList[nIndex];
      ASSERT(pszMoveTemp);        // what we are moving ???
      m_ppszList[nIndex] = psz;
      psz = pszMoveTemp;
      nIndex++;
    }

    // save last entry if we have not reached max entries count
    if (nIndex < m_nMaxEntries)
    {
      m_ppszList[nIndex] = psz;
      m_nCount++;
    }
  }

  hr = WriteToRegistry();

  return SUCCEEDED(hr)?S_OK:hr;
}

HRESULT CMRUList::WriteToRegistry()
{
  TCHAR NameBuffer[256];
  unsigned int nIndex;

  LONG nLastError = ERROR_SUCCESS;

  // Write the new MRU list into registry
  for (nIndex = 0 ; nIndex < m_nCount ; nIndex++)
  {
    _ultot(nIndex,NameBuffer,ENTRY_BASE);
    LONG nError = RegSetValueEx(m_hKey,NameBuffer,0,REG_SZ,(const BYTE *)m_ppszList[nIndex],(_tcslen(m_ppszList[nIndex])+1)*sizeof(TCHAR));
    if (nError != ERROR_SUCCESS)
      nLastError = nError;
  }

  return HRESULT_FROM_WIN32(nLastError);
}

unsigned int CMRUList::GetCount()
{
  return m_nCount;
}

LPCTSTR CMRUList::GetEntry(unsigned int nIndex)
{
  if ((nIndex >= m_nCount)||(m_ppszList == NULL))
  {
    ASSERT(FALSE);
    return _T("Error");
  }

  return m_ppszList[nIndex];
}
