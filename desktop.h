// $Id: desktop.h,v 1.1 2002/05/29 22:06:50 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef _DESKTOP_H_bc78ffe3_8dac_4e21_9c3f_b49a266bef53__INCLUDED
#define _DESKTOP_H_bc78ffe3_8dac_4e21_9c3f_b49a266bef53__INCLUDED

#include "DesktopSwitcher.h"
#include "SwitcherWindow.h"

class CDesktop
{
public:
  CDesktop(CDesktopSwitcher * pDesktopSwitcher, const char *pszDesktopName, const char *pszDesktopShowName, HRESULT& rhrError);
  const char *GetDesktopName();
  HRESULT SwitchTo();
  void Exit();
  void WindowToggleVisible();
  void Run(const char *pszCommand);
  void RunPrompt();

private:
  ~CDesktop();
  static DWORD WINAPI ThreadProc(LPVOID lpParameter);
  DWORD ThreadProc();

  HDESK m_hDesktop;
  CDesktopSwitcher *m_pDesktopSwitcher;
  CSwitcherWindow *m_pWnd;
  char *m_pszDesktopShowName;
  char *m_pszDesktopName;
};

#endif // #ifndef _DESKTOP_H_bc78ffe3_8dac_4e21_9c3f_b49a266bef53__INCLUDED
