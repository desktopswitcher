// $Id: switcherwindow.cpp,v 1.3 2003/08/03 16:56:29 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "ph.h"
#include "SwitcherWindow.h"
#include "DesktopSwitcher.h"
#include "Desktop.h"
#include "RunDialog.h"
#include "resource.h"
#include "png.h"

#define BACKGROUD_COLOR  RGB(96,96,96)
#define TEXT_COLOR       RGB(220,220,220)
#define DOCK             10

CSwitcherWindow::CSwitcherWindow(CDesktopSwitcher *pDesktopSwitcher, CDesktop *pDesktop)
{
  m_pDesktopSwitcher = pDesktopSwitcher;
  m_pDesktop = pDesktop;
  m_pMoveWnd = NULL;
}

CSwitcherWindow::~CSwitcherWindow()
{
}

BOOL CSwitcherWindow::Create()
{
  RECT rect = {0,0,g_Image.GetWidth(),g_Image.GetHeight()};
  if (!CWindowImpl<CSwitcherWindow>::Create(NULL, // Desktop is our parent
                                            rect, // Window rectangle
                                            "rDesktop Switcher", // Window name
                                            WS_POPUP, // Style
                                            WS_EX_TOPMOST|WS_EX_TOOLWINDOW|WS_EX_LAYERED, // Extended style
                                            0))
  {
    DWORD dwError = GetLastError();
    return FALSE;
  }

  m_pDesktopSwitcher->RegisterHotKeys(m_hWnd);

  DrawWindow();

  return TRUE;
}

LRESULT CSwitcherWindow::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
  m_pDesktopSwitcher->Exit();
  return 0;
}

LRESULT CSwitcherWindow::OnHotKey(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
  WORD wVKey = HIWORD(lParam);
  WORD wModifiers = LOWORD(lParam);
  m_pDesktopSwitcher->HotKeyPressed(wVKey,wModifiers);

  return 0;
}

void CSwitcherWindow::ToggleVisible()
{
  ShowWindow(IsWindowVisible()?SW_HIDE:SW_SHOW);
}

LRESULT CSwitcherWindow::OnRButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
  unsigned int n = 0;
  HMENU hMenu = CreatePopupMenu();
  MENUITEMINFO mi;
  mi.cbSize = sizeof(MENUITEMINFO);
  mi.fMask = MIIM_TYPE|MIIM_ID;
  mi.fType = MFT_STRING;

  mi.dwTypeData = "Run...";
  mi.wID = ID_RUN;
  InsertMenuItem(hMenu,n++,TRUE,&mi);

  mi.dwTypeData = "Exit";
  mi.wID = ID_EXIT;
  InsertMenuItem(hMenu,n++,TRUE,&mi);

  POINT pt;
  pt.x = LOWORD(lParam);
  pt.y = HIWORD(lParam);

  TrackPopupMenu(hMenu,TPM_LEFTALIGN,pt.x,pt.y,0,m_hWnd,NULL);
  return 0;
}

void CSwitcherWindow::DrawWindow()
{
  const char *pszDesktopName = m_pDesktop->GetDesktopName();
  HDC hScreenDC = NULL;
  HDC hMemoryDC = NULL;
  HBITMAP hBitmap = NULL;
  RECT r;

  GetWindowRect(&r);

  // setup the blend function
  BLENDFUNCTION blendPixelFunction = { AC_SRC_OVER, 0, 0xFF,AC_SRC_ALPHA };

  POINT ptSrc = {0,0}; // start point of the copy from hMemoryDC to hScreenDC
  POINT ptWindowScreenPosition = {r.left,r.top};
  SIZE szWindow = {g_Image.GetWidth(), g_Image.GetHeight()};

  if (pszDesktopName == NULL)
    goto Exit;

  hScreenDC = ::GetDC(m_hWnd);
  if (hScreenDC == NULL)
    goto Exit;

  hMemoryDC = ::CreateCompatibleDC(hScreenDC);
  if (hMemoryDC == NULL)
    goto Exit;

  hBitmap = g_Image.CreateBitmap(hMemoryDC);
  if (hBitmap == NULL)
    goto Exit;

  if (SelectObject(hMemoryDC,hBitmap) == NULL)
    goto Exit;

  // perform the alpha blend
  UpdateLayeredWindow(m_hWnd,
                      hScreenDC,
                      &ptWindowScreenPosition,
                      &szWindow,
                      hMemoryDC,
                      &ptSrc,
                      0,
                      &blendPixelFunction,
                      ULW_ALPHA);

 Exit:
  if (hMemoryDC)
    VERIFY(DeleteDC(hMemoryDC));

  if (hScreenDC)
    VERIFY(ReleaseDC(hScreenDC));

  if (hBitmap)
    VERIFY(DeleteObject(hBitmap));
}

LRESULT CSwitcherWindow::OnNCHitText(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
  return HTCAPTION;
}

LRESULT CSwitcherWindow::OnRun(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
  CRunDialog dlg;

  const char *szExec = dlg.DoModal(m_hWnd);
  if (szExec)
    m_pDesktop->Run(szExec);
  return 0;
}

LRESULT CSwitcherWindow::OnExit(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
  m_pDesktopSwitcher->Exit();
  return 0;
}

void CSwitcherWindow::Destroy()
{
  VERIFY(DestroyWindow());
}

void CSwitcherWindow::OnRun()
{
  PostMessage(WM_COMMAND,MAKEWPARAM(ID_RUN,1),NULL);
}

LRESULT CSwitcherWindow::OnExitSizeMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{/*
  RECT rect;
  VERIFY(GetWindowRect(&rect));
  if (rect.left > -DOCK && rect.left < DOCK)
  {
    rect.right -= rect.left;
    rect.left = 0;
  }

  if (rect.top+11 > -DOCK && rect.top+11 < DOCK)
  {
    rect.bottom -= rect.top+11;
    rect.top = -11;
  }

  VERIFY(SetWindowPos(NULL,rect.left,rect.top,10,10,SWP_NOACTIVATE|SWP_NOOWNERZORDER|SWP_NOSIZE|SWP_NOZORDER|SWP_SHOWWINDOW));*/
  return 0;
}
