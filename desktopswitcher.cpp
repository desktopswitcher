// $Id: desktopswitcher.cpp,v 1.2 2003/08/03 16:56:29 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "ph.h"
#include "DesktopSwitcher.h"
#include "Desktop.h"

CDesktopSwitcher::CDesktopSwitcher()
{
  m_ppDesktops = NULL;
  m_hExitEvent = NULL;
}

CDesktopSwitcher::~CDesktopSwitcher()
{
  ASSERT(!m_ppDesktops);
  if (m_ppDesktops)
    delete m_ppDesktops;

  ASSERT(!m_hExitEvent);
  if (m_hExitEvent)
    VERIFY(CloseHandle(m_hExitEvent));
}

HRESULT CDesktopSwitcher::Go(unsigned int nDesktops)
{
  DWORD dwError;

  if (m_hExitEvent)
    return E_UNEXPECTED;        // call this method only once

  if (nDesktops <= 1 || nDesktops > 12)
    return E_INVALIDARG;

  m_nDesktops = nDesktops;

  m_hExitEvent = CreateEvent(NULL,TRUE,FALSE,NULL);
  if (!m_hExitEvent)
  {
    dwError = GetLastError();
    return HRESULT_FROM_WIN32(dwError);
  }

  m_ppDesktops = new CDesktop * [nDesktops];
  if (!m_ppDesktops)
    return E_OUTOFMEMORY;

  m_nCurrentDesktopIndex = 0;

  HRESULT hr;
  char pszDesktopName[256] = "";

  HKEY hKey;
  // open/create the key
  RegCreateKeyEx(HKEY_CURRENT_USER, DESKTOP_NAMES_KEY, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE|KEY_SET_VALUE,NULL,&hKey,NULL);
  char NameBuffer[256];

  for (unsigned int i = 0 ; i < nDesktops ; i++)
  {
    // try to get desktop name from registry
    _ultot(i+1,NameBuffer,10);
    DWORD dwSize = 256;
    DWORD dwType;
    if (hKey == NULL || RegQueryValueEx(hKey,NameBuffer,NULL,&dwType,(BYTE *)pszDesktopName,&dwSize) != ERROR_SUCCESS && dwType != REG_SZ)
      sprintf(pszDesktopName,"Desktop %u",i+1);

    m_ppDesktops[i] = new CDesktop(this,i?pszDesktopName:"Default",pszDesktopName,hr);
    if (!m_ppDesktops[i])
    {
      Exit();
      return E_OUTOFMEMORY;
    }

    if (FAILED(hr))
    {
      Exit();
      return E_OUTOFMEMORY;
    }
  }

  switch(WaitForSingleObject(m_hExitEvent,INFINITE))
  {
  case WAIT_OBJECT_0:
    break;
  case WAIT_FAILED:
    {
      dwError = GetLastError();
      return HRESULT_FROM_WIN32(dwError);
    }
  default:
    ASSERT(FALSE);
    return E_UNEXPECTED;
  }

  m_ppDesktops[0]->SwitchTo();

  for (i = 0 ; i < nDesktops ; i ++)
    m_ppDesktops[i]->Exit();

  delete m_ppDesktops;
  m_ppDesktops = NULL;

  VERIFY(CloseHandle(m_hExitEvent));
  m_hExitEvent= NULL;

  return S_OK;
}

CDesktop * CDesktopSwitcher::GetDefaultDesktop()
{
  if (!m_ppDesktops)
    return NULL;

  return m_ppDesktops[0];
}

void CDesktopSwitcher::Exit()
{
  ASSERT(m_hExitEvent);
  VERIFY(SetEvent(m_hExitEvent));
}

HRESULT CDesktopSwitcher::HotKeyPressed(WORD wVKey, WORD wModifiers)
{
  HRESULT hr;
  if (wVKey >= VK_F1 && wVKey < VK_F1 + m_nDesktops && wModifiers == (MOD_ALT|MOD_CONTROL))
  {
    m_nCurrentDesktopIndex = wVKey - VK_F1;
    hr = m_ppDesktops[m_nCurrentDesktopIndex]->SwitchTo();
    if (FAILED(hr))
      return hr;
  }
  else if (wVKey == VK_RETURN && wModifiers == MOD_WIN)
  {
    for (unsigned int i = 0 ; i < m_nDesktops ; i ++)
      m_ppDesktops[i]->WindowToggleVisible();

    return S_OK;
  }
  else if (wVKey == VK_ADD && wModifiers == MOD_WIN)
  {
    m_nCurrentDesktopIndex = (m_nCurrentDesktopIndex+1) % m_nDesktops;
    hr = m_ppDesktops[m_nCurrentDesktopIndex]->SwitchTo();
    if (FAILED(hr))
      return hr;

    return S_OK;
  }
  else if (wVKey == VK_SUBTRACT && wModifiers == MOD_WIN)
  {
    if (m_nCurrentDesktopIndex)
      m_nCurrentDesktopIndex--;
    else
      m_nCurrentDesktopIndex = m_nDesktops-1;

    hr = m_ppDesktops[m_nCurrentDesktopIndex]->SwitchTo();
    if (FAILED(hr))
      return hr;

    return S_OK;
  }
  else if (wVKey == 'R' && wModifiers == (MOD_WIN|MOD_SHIFT))
  {
    m_ppDesktops[m_nCurrentDesktopIndex]->RunPrompt();
    return S_OK;
  }
  else if (wVKey == VK_ESCAPE && wModifiers == (MOD_WIN))
  {
    Exit();
    return S_OK;
  }

  return S_FALSE;
}

void CDesktopSwitcher::RegisterHotKeys(HWND hWnd)
{
  for (unsigned int i=0 ; i < m_nDesktops ; i++)
    RegisterHotKey (hWnd, VK_F1+i, MOD_ALT|MOD_CONTROL, VK_F1+i);

  RegisterHotKey(hWnd, VK_RETURN, MOD_WIN, VK_RETURN);
  RegisterHotKey(hWnd, VK_ADD, MOD_WIN, VK_ADD);
  RegisterHotKey(hWnd, VK_SUBTRACT, MOD_WIN, VK_SUBTRACT);
  RegisterHotKey(hWnd, 'R', MOD_WIN|MOD_SHIFT, 'R');
  RegisterHotKey(hWnd, VK_ESCAPE, MOD_WIN, VK_ESCAPE);
}
