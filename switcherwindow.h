// $Id: switcherwindow.h,v 1.3 2003/08/03 16:56:29 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef _SWITCHER_WINDOW_H_f8ade2f7_4bdf_4b29_9e91_fd26a09ff35d__INCLUDED
#define _SWITCHER_WINDOW_H_f8ade2f7_4bdf_4b29_9e91_fd26a09ff35d__INCLUDED

#include "switcher.h"
#include "ShadowWindow.h"
class CDesktopSwitcher;
class CDesktop;

// Preparation for the function we want to import from USER32.DLL
typedef BOOL (WINAPI *lpfnUpdateLayeredWindow)(HWND hwnd,             // handle to layered window
                                               HDC hdcDst,            // handle to screen DC
                                               POINT *pptDst,         // new screen position
                                               SIZE *psize,           // new size of the layered window
                                               HDC hdcSrc,            // handle to surface DC
                                               POINT *pptSrc,         // layer position
                                               COLORREF crKey,        // color key
                                               BLENDFUNCTION *pblend, // blend function
                                               DWORD dwFlags);        // options

class CSwitcherWindow : public CWindowImpl <CSwitcherWindow>
{
public:
  CSwitcherWindow(CDesktopSwitcher *pDesktopSwitcher, CDesktop *pDesktop);
  ~CSwitcherWindow();
  BOOL Create();
  void Destroy();
  void ToggleVisible();
  void OnRun();
  void DrawWindow();

  BEGIN_MSG_MAP(CSwitcherWindow)
    MESSAGE_HANDLER(WM_CLOSE,OnClose)
    MESSAGE_HANDLER(WM_HOTKEY,OnHotKey)
    MESSAGE_HANDLER(WM_NCRBUTTONDOWN,OnRButtonDown)
    MESSAGE_HANDLER(WM_EXITSIZEMOVE,OnExitSizeMove)
    MESSAGE_HANDLER(WM_NCHITTEST,OnNCHitText)
    COMMAND_ID_HANDLER(ID_RUN,OnRun)
    COMMAND_ID_HANDLER(ID_EXIT,OnExit)
  END_MSG_MAP()

protected:
  LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
  LRESULT OnHotKey(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
  LRESULT OnRButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
  LRESULT OnExitSizeMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
  LRESULT OnNCHitText(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

  LRESULT OnRun(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
  LRESULT OnExit(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

private:
  CDesktopSwitcher *m_pDesktopSwitcher;
  CDesktop *m_pDesktop;

  CShadowWindow *m_pMoveWnd;
  POINT m_ptMoveBegin;
};

#endif // #ifndef _SWITCHER_WINDOW_H_f8ade2f7_4bdf_4b29_9e91_fd26a09ff35d__INCLUDED
