// $Id: mru.h,v 1.1 2002/05/29 22:06:50 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef MRU_H__fa247b99_e1f7_4216_a9e6_16b96fd237a7__INCLUDED
#define MRU_H__fa247b99_e1f7_4216_a9e6_16b96fd237a7__INCLUDED

class CMRUList
{
public:
  CMRUList(unsigned int nMaxEntries, LPCTSTR psz_HKCU_MRUKey, HRESULT& rhr);
  ~CMRUList();
  HRESULT Init();
  HRESULT Add(LPCTSTR pszEntry);
  unsigned int GetCount();
  LPCTSTR GetEntry(unsigned int nIndex);

private:
  HRESULT WriteToRegistry();

  LPTSTR *m_ppszList;
  unsigned int m_nCount;
  unsigned int m_nMaxEntries;
  HKEY m_hKey;
};

#endif // #ifndef MRU_H__fa247b99_e1f7_4216_a9e6_16b96fd237a7__INCLUDED
