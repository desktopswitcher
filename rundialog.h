// $Id: rundialog.h,v 1.1 2002/05/29 22:06:50 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef _RUN_DIALOG_H__70660b7b_3732_4819_adfc_3949db2a78e8__INCLUDED
#define _RUN_DIALOG_H__70660b7b_3732_4819_adfc_3949db2a78e8__INCLUDED

#define MAX_FILENAME_SIZE 10240

#include "MRU.h"

class CRunDialog
{
public:
  CRunDialog(const char *pszDefault = "cmd");
  ~CRunDialog();
  const char * DoModal(HWND hwnd);
private:
  static int __stdcall DialogProc(HWND hwndDlg, UINT nMsg, WPARAM wParam, LPARAM lParam);
  char m_FilenameBuffer[MAX_FILENAME_SIZE];
  CMRUList *m_pMRU;
};

#endif // ifndef _RUN_DIALOG_H__70660b7b_3732_4819_adfc_3949db2a78e8__INCLUDED
