/////////////////////////////////////////////////////////////////////////////////
//
// $Id: png.cpp,v 1.4 2003/08/03 16:56:29 nedko Exp $
//
// DESCRIPTION:
//  
// NOTES:
//  
/////////////////////////////////////////////////////////////////////////////////

#include "ph.h"
#include "png.h"

#define NOT_LOADED_BITMAP_HEIGHT  30
#define NOT_LOADED_BITMAP_WIDTH  400
#define NOT_LOADED_BITMAP_RED   0x80
#define NOT_LOADED_BITMAP_GREEN 0xE0
#define NOT_LOADED_BITMAP_BLUE  0xFF
#define NOT_LOADED_BITMAP_ALPHA 0xE0

CPNGImage::CPNGImage()
{
  m_nWidth = NOT_LOADED_BITMAP_WIDTH;
  m_nHeight = NOT_LOADED_BITMAP_HEIGHT;
  m_pData = NULL;
}

CPNGImage::~CPNGImage()
{
  if (m_pData)
    delete m_pData;
}

void PNGAPI
CPNGImage::PNG_ErrorHandler PNGARG((png_structp s, png_const_charp str))
{
  ::MessageBox(NULL,str,"PNG Error",MB_OK);
}

bool
CPNGImage::LoadPNGImage(const char *pszPath)
{
  png_structp png_ptr;
  png_infop info_ptr;
  unsigned int sig_read = 0;

  FILE *fp = fopen(pszPath, "rb");
  if (!fp)
  {
    return false;
  }

  png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
                                   NULL,
                                   PNG_ErrorHandler,
                                   PNG_ErrorHandler);

  if (png_ptr == NULL)
  {
    fclose(fp);
    return false;
  }

  info_ptr = png_create_info_struct(png_ptr);
  if (info_ptr == NULL)
  {
    fclose(fp);
    png_destroy_read_struct(&png_ptr, png_infopp_NULL, png_infopp_NULL);
    return false;
  }

  if (setjmp(png_jmpbuf(png_ptr)))
  {
    /* Free all of the memory associated with the png_ptr and info_ptr */
    png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
    fclose(fp);
    /* If we get here, we had a problem reading the file */
    return false;
  }

  png_init_io(png_ptr, fp);

  /* If we have already read some of the signature */
  png_set_sig_bytes(png_ptr, sig_read);

  png_read_png(png_ptr,
               info_ptr,
               PNG_TRANSFORM_IDENTITY,
               png_voidp_NULL);

  m_nHeight = png_get_image_height(png_ptr, info_ptr);
  m_nWidth = png_get_image_width(png_ptr, info_ptr);

  m_pData = new unsigned char[m_nWidth*m_nHeight*4];
  png_bytep *row_pointers = png_get_rows(png_ptr, info_ptr);
  for (unsigned int y = 0 ; y < m_nHeight; y++)
  {
    memcpy(m_pData + m_nWidth * 4 * y,row_pointers[y],m_nWidth*4);
  }

  PremultiplyAlpha(m_nWidth, m_nHeight, m_pData);

  /* clean up after the read, and free any memory allocated - REQUIRED */
  png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);

  /* close the file */
  fclose(fp);

  return true;
}

unsigned int
CPNGImage::GetWidth()
{
  return m_nWidth;
}

unsigned int
CPNGImage::GetHeight()
{
  return m_nHeight;
}

HBITMAP
CPNGImage::CreateBitmap(HDC hMemoryDC)
{
  HBITMAP hBitmap;
  BITMAPINFO bi;
  ZeroMemory(&bi,sizeof(bi));
  bi.bmiHeader.biSize = sizeof(bi.bmiHeader);
  bi.bmiHeader.biWidth = m_nWidth;
  bi.bmiHeader.biHeight = m_nHeight;
  bi.bmiHeader.biPlanes = 1;
  bi.bmiHeader.biBitCount = 32;
  bi.bmiHeader.biCompression = BI_RGB;

  void *pDIBBits;

  hBitmap = ::CreateDIBSection(hMemoryDC,&bi,DIB_RGB_COLORS,&pDIBBits,NULL,0);

  if (m_pData)
  {
    memcpy(pDIBBits, m_pData, m_nWidth * m_nHeight * 4);
  }
  else
  {
    unsigned char *pData = GenerateNotLoadedBitmapData(m_nWidth, m_nHeight);

    memcpy(pDIBBits, pData, m_nWidth * m_nHeight * 4);

    delete pData;
  }

  return hBitmap;
}

unsigned char *
CPNGImage::GenerateNotLoadedBitmapData(unsigned int nWidth, unsigned int nHeight)
{
  unsigned int x,y;
  unsigned char *pPixel;

  unsigned char *pData = new unsigned char[nWidth*nHeight*4];
  for (y = 0 ; y < nHeight; y++)
  {
    pPixel= pData + nWidth * 4 * y;

    for (x = 0 ; x < nWidth ; x++)
    {
      pPixel[0]= NOT_LOADED_BITMAP_BLUE;
      pPixel[1]= NOT_LOADED_BITMAP_GREEN;
      pPixel[2]= NOT_LOADED_BITMAP_RED;
      pPixel[3]= NOT_LOADED_BITMAP_ALPHA;

      pPixel+= 4;
    }
  }

  PremultiplyAlpha(nWidth, nHeight, pData);

  return pData;
}

void
CPNGImage::PremultiplyAlpha(unsigned int nWidth,
                            unsigned int nHeight,
                            unsigned char *pData)
{
  unsigned int x,y;
  unsigned char *pPixel;

  for (y = 0 ; y < nHeight; y++)
  {
    pPixel = pData + nWidth * 4 * y;

    for (x = 0 ; x < nWidth ; x++)
    {
      pPixel[0]= (unsigned __int8)(((unsigned int)pPixel[0])*pPixel[3]/255);
      pPixel[1]= (unsigned __int8)(((unsigned int)pPixel[1])*pPixel[3]/255);
      pPixel[2]= (unsigned __int8)(((unsigned int)pPixel[2])*pPixel[3]/255);
      pPixel+= 4;
    }
  }
}

/////////////////////////////////////////////////////////////////////////////////
//
// Modifications log:
//
// !!! WARNING !!! Following lines are automatically updated by the CVS system.
//
//   $Log: png.cpp,v $
//   Revision 1.4  2003/08/03 16:56:29  nedko
//   Flush current work
//
//   Revision 1.3  2002/06/29 16:00:28  nedko
//   *** empty log message ***
//
//   Revision 1.2  2002/06/29 15:20:10  nedko
//   *** empty log message ***
//
//   Revision 1.1  2002/06/29 15:03:33  nedko
//   *** empty log message ***
//
/////////////////////////////////////////////////////////////////////////////////
