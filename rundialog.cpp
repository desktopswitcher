// $Id: rundialog.cpp,v 1.1 2002/05/29 22:06:50 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "ph.h"
#include "switcher.h"
#include "RunDialog.h"
#include "resource.h"

CRunDialog::CRunDialog(const char *pszDefault)
{
  strcpy(m_FilenameBuffer,pszDefault);

  HRESULT hr = S_OK;
  m_pMRU = new CMRUList(RUN_MRU_MAX, RUN_MRU_KEY, hr);
  if (FAILED(hr))
  {
    delete m_pMRU;
    m_pMRU = NULL;
  }
}

CRunDialog::~CRunDialog()
{
}

const char * CRunDialog::DoModal(HWND hwnd)
{
  if (DialogBoxParam(_Module.GetModuleInstance(),MAKEINTRESOURCE(IDD_DIALOG_RUN),hwnd,DialogProc,(LPARAM)this) == IDOK)
    return m_FilenameBuffer;
  else
    return NULL;
}

int __stdcall CRunDialog::DialogProc(HWND hwndDlg, UINT nMsg, WPARAM wParam, LPARAM lParam)
{
  CRunDialog *pThis = (CRunDialog *)GetWindowLong(hwndDlg,GWL_USERDATA);
  switch(nMsg)
  {
  case WM_INITDIALOG:
    {
      SetForegroundWindow(hwndDlg);
      pThis = (CRunDialog *)lParam;
      SetWindowLong(hwndDlg,GWL_USERDATA,(LPARAM)pThis);
      HWND hwndCombo = GetDlgItem(hwndDlg,IDC_COMBO_COMMAND);
      if (hwndCombo && pThis->m_pMRU)
      {
        for (unsigned int i = 0; i < pThis->m_pMRU->GetCount(); i ++)
          SendMessage(hwndCombo,CB_INSERTSTRING,-1,(LPARAM)pThis->m_pMRU->GetEntry(i));
      }
      SendMessage(hwndCombo,CB_SETCURSEL,0,0);
      SendMessage(hwndCombo,CB_SETDROPPEDWIDTH,600,0);
    }
    return TRUE;
  case WM_COMMAND:
    switch(LOWORD(wParam))
    {
    case IDC_BUTTON_BROWSE:
      {
        OPENFILENAME ofn;
        ofn.lStructSize = sizeof(OPENFILENAME);
        ofn.hwndOwner = hwndDlg;
        ofn.hInstance = NULL;
        ofn.lpstrFilter = "Executable Files (*.exe;*.com;*.bat)\0*.exe;*.com;*.bat\0All Files (*.*)\0*.*\0";
        ofn.lpstrCustomFilter = NULL;
        ofn.nMaxCustFilter = 0;
        ofn.nFilterIndex = 1;
        pThis->m_FilenameBuffer[0] = 0;
        ofn.lpstrFile = pThis->m_FilenameBuffer;
        ofn.nMaxFile = MAX_FILENAME_SIZE;
        ofn.lpstrFileTitle = NULL;
        ofn.nMaxFileTitle = 0;
        ofn.lpstrInitialDir = NULL;
        ofn.lpstrTitle = "Please select file to run";
        ofn.Flags = OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_PATHMUSTEXIST;
        ofn.nFileOffset = 0;
        ofn.nFileExtension = 0;
        ofn.lpstrDefExt = NULL;
        ofn.lCustData = (LPARAM)pThis;
        ofn.lpfnHook = NULL;
        ofn.lpTemplateName = NULL;
#if (_WIN32_WINNT >= 0x0500)
        ofn.pvReserved = NULL;
        ofn.dwReserved = 0;
        ofn.FlagsEx  = 0;
#endif
        if (GetOpenFileName(&ofn))
          SetWindowText(GetDlgItem(hwndDlg,IDC_COMBO_COMMAND),pThis->m_FilenameBuffer);
      }
      return TRUE;
    case IDOK:
      if (!GetWindowText(GetDlgItem(hwndDlg,IDC_COMBO_COMMAND),pThis->m_FilenameBuffer,MAX_FILENAME_SIZE))
      {
        strcpy(pThis->m_FilenameBuffer,"cmd");
      }
      else if (pThis->m_pMRU)
      {
        VERIFY(SUCCEEDED(pThis->m_pMRU->Add(pThis->m_FilenameBuffer)));
      }
    case IDCANCEL:
      EndDialog(hwndDlg,LOWORD(wParam));
      return TRUE;
    }
  default:
    return FALSE;
  }
}
