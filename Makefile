# $Id: Makefile,v 1.5 2003/08/03 16:56:29 nedko Exp $
#
# Desktop Switcher
# Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

debugflags = /D_DEBUG /Zi /Od
linkdebugflags = /DEBUG
crtlib = LIBCMTD.LIB
#crtlib = LIBCMT.LIB

cc = cl -c -nologo $(debugflags)
link = link $(crtlib) /NODEFAULTLIB:LIBC $(linkdebugflags)

linklibs = gdi32.lib comdlg32.lib Msimg32.lib libpng.a libz.a

all: switcher.exe

switcher.res: switcher.rc switcher.h
	rc -r -fo switcher.res switcher.rc

switcher.obj: ph.h switcher.cpp switcher.h DesktopSwitcher.h png.h
	$(cc) switcher.cpp

RunDialog.obj: ph.h RunDialog.cpp RunDialog.h
	$(cc) RunDialog.cpp

SwitcherWindow.obj: ph.h SwitcherWindow.cpp SwitcherWindow.h DesktopSwitcher.h RunDialog.h switcher.h ShadowWindow.h png.h
	$(cc) SwitcherWindow.cpp

Desktop.obj: ph.h Desktop.cpp Desktop.h SwitcherWindow.h switcher.h ShadowWindow.h
	$(cc) Desktop.cpp

DesktopSwitcher.obj: ph.h DesktopSwitcher.cpp DesktopSwitcher.h Desktop.h
	$(cc) DesktopSwitcher.cpp

MRU.obj: ph.h MRU.cpp MRU.h
	$(cc) MRU.cpp

png.obj: ph.h png.cpp png.h
	$(cc) png.cpp

#ShadowWindow.obj: ph.h ShadowWindow.cpp ShadowWindow.h
#	$(cc) ShadowWindow.cpp

LINK_INPUT=switcher.obj RunDialog.obj SwitcherWindow.obj Desktop.obj DesktopSwitcher.obj MRU.obj png.obj switcher.res

switcher.exe: $(LINK_INPUT)
	$(link) -out:switcher.exe \
	$(LINK_INPUT) $(linklibs)


clean:
	del *.exe
	del *.obj
	del *.aps
	del *.res
