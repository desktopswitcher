// $Id: shadowwindow.cpp,v 1.1 2002/05/29 22:06:50 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "ph.h"
#include "ShadowWindow.h"
#include "SwitcherWindow.h"

CShadowWindow::CShadowWindow(CSwitcherWindow *pWndSource)
{
  m_pWndSource = pWndSource;
  RECT rect;

  pWndSource->GetWindowRect(&rect);
  m_nWidth = rect.right-rect.left;
  m_nHeight = rect.bottom-rect.top;

  DWORD dwStyle = pWndSource->GetWindowLong(GWL_STYLE);
  DWORD dwExStyle = pWndSource->GetWindowLong(GWL_EXSTYLE);

  CWindowImpl<CShadowWindow>::Create(NULL, // Desktop is our parent
                                     rect, // Window rectangle
                                     "Shadow", // Window name
                                     dwStyle&(~WS_VISIBLE), // Style
                                     dwExStyle, // Extended style
                                     0);

  HRGN hRgn = NULL;
  pWndSource->GetWindowRgn(hRgn);
  VERIFY(SetWindowRgn(hRgn,TRUE));
}

CShadowWindow::~CShadowWindow()
{
  if (m_hWnd)
    DestroyWindow();
}

void CShadowWindow::Move(unsigned int x, unsigned int y)
{
  ShowWindow(SW_SHOW);

  MoveWindow(x,y,m_nWidth,m_nHeight,TRUE);
}

LRESULT CShadowWindow::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
  m_pWndSource->DrawWindow(m_hWnd);

  return 0;
}
