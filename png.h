/////////////////////////////////////////////////////////////////////////////////
//
// $Id: png.h,v 1.2 2003/08/03 16:56:29 nedko Exp $
//
// DESCRIPTION:
//  
// NOTES:
//  
/////////////////////////////////////////////////////////////////////////////////

#ifndef PNG_H__76DD7C22_846A_4AE0_B01C_813DBDADE3C4__INCLUDED
#define PNG_H__76DD7C22_846A_4AE0_B01C_813DBDADE3C4__INCLUDED

class CPNGImage
{
public:
  CPNGImage();
  ~CPNGImage();

  bool LoadPNGImage(const char *pszPath);
  unsigned int GetWidth();
  unsigned int GetHeight();
  HBITMAP CreateBitmap(HDC hMemoryDC);
private:
  static void PNGAPI PNG_ErrorHandler PNGARG((png_structp s, png_const_charp str));
  static void PremultiplyAlpha(unsigned int nWidth, unsigned int nHeight, unsigned char *pData);
  static unsigned char * GenerateNotLoadedBitmapData(unsigned int nWidth, unsigned int nHeight);

private:
  unsigned int m_nWidth;
  unsigned int m_nHeight;
  unsigned char *m_pData;
};

extern CPNGImage g_Image;

#endif // #ifndef PNG_H__76DD7C22_846A_4AE0_B01C_813DBDADE3C4__INCLUDED

/////////////////////////////////////////////////////////////////////////////////
//
// Modifications log:
//
// !!! WARNING !!! Following lines are automatically updated by the CVS system.
//
//   $Log: png.h,v $
//   Revision 1.2  2003/08/03 16:56:29  nedko
//   Flush current work
//
//   Revision 1.1  2002/06/29 15:03:33  nedko
//   *** empty log message ***
//
/////////////////////////////////////////////////////////////////////////////////
