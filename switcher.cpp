// $Id: switcher.cpp,v 1.3 2002/06/29 16:00:28 nedko Exp $
//
// Desktop Switcher
// Copyright (C) 2000,2001,2002 Nedko Arnaudov <nedko@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "ph.h"

#include "switcher.h"
#include "DesktopSwitcher.h"
#include "png.h"

CComModule _Module;
CPNGImage g_Image;

BEGIN_OBJECT_MAP(ObjectMap)
END_OBJECT_MAP()

int CALLBACK WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine,
                     int nCmdShow)
{
  _Module.Init(ObjectMap,hInstance);
  int cThreads;                // number of desktops

  // parse command line to determine number of desktops
  // Assume 9 threads

  cThreads = 12;
  lpCmdLine = GetCommandLineA();

  //
  // Get past .exe name
  //
  while (*lpCmdLine != ' ' && *lpCmdLine != 0)
    lpCmdLine++;

  //
  // Find the parameters
  //
  while (*lpCmdLine != 0)
  {

    // Eat white space

    if (*lpCmdLine == ' ')
    {
      lpCmdLine++;
      continue;
    }

    //
    // Do we have a dash? If not, just exit the loop
    //
    if (*lpCmdLine++ != '-')
      break;

    switch (*lpCmdLine++)
    {
    case 't':
    case 'T':
      //
      // How many threads?
      //

      while (*lpCmdLine == ' ')
        lpCmdLine++;

      if (*lpCmdLine == 0 || *lpCmdLine == '-')
        continue;

      cThreads = 0;
      while (*lpCmdLine >= '0' && *lpCmdLine <= '9')
        cThreads = cThreads * 10 + (*lpCmdLine++ - 0x30);

      break;

    }
  }

  g_Image.LoadPNGImage("C:\\topka.png");

  CDesktopSwitcher TheSwitcher;
  HRESULT hr = TheSwitcher.Go(cThreads);
  if (FAILED(hr))
  {
    char Buffer[256];
    sprintf(Buffer,"Failed to initialize. Error is 0x%X",hr);
    ::MessageBox(NULL,Buffer,"Desktop Switcher",MB_OK|MB_ICONSTOP);
  }

  return 0;
}
